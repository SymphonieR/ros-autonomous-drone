#include <ros/ros.h>
#define OPENCV_TRAITS_ENABLE_DEPRECATED
#include <opencv2/opencv.hpp>
#include <visualization_msgs/Marker.h>
#include <Eigen/Core>
#include <floor_plane_ransac/Param.h>
#include <geometry_msgs/Point32.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/Float32.h>

class TreasureDetection {
	protected:
		ros::Subscriber signal_sub_;
		ros::Subscriber position_sub_;
		ros::NodeHandle nh_;
	        std::vector<Eigen::Vector2d> cylinders;	
		std::vector<float> sigmas;
		ros::Publisher marker_pub_;

		Eigen::Vector2d position;

	protected:
		void position_callback(const geometry_msgs::Point32 msg){
			position << msg.x, msg.y;
		}

		void signal_callback(const std_msgs::Float32 msg) {
			float signal_min = 0.98;
			float signal_max = 1;
			if (msg.data > 0.98){
				ROS_INFO("Mine detected: %f, %f", position[0], position[1]);
            			unsigned int nc = cylinders.size();
				bool unique = true;
				unsigned int j = 0;

				float sigma = ((msg.data - signal_min)/(signal_max-signal_min))*4.0 + 2.0;
 				while (unique  && j<nc){		
					Eigen::Vector2d CC = cylinders[j];
					unique = ((CC-position).norm() > 2);
					j++;
				}

				if(unique){
					ROS_INFO("new cylinder found");
					cylinders.push_back(position);
					sigmas.push_back(sigma);
				} else {
					j--;
					ROS_INFO("will join %d", j);
                                        float sigma0 = sigmas[j];
                                        sigmas[j] = 1.0/(1.0/sigmas[j]+1.0/sigma);
					ROS_INFO("old: %f, %f, %f", cylinders[j][0], cylinders[j][1], sigma0);
                                        cylinders[j][0] = sigmas[j] * (cylinders[j][0]/sigma0 + position[0]/sigma);
                                        cylinders[j][1] = sigmas[j] * (cylinders[j][1]/sigma0 + position[1]/sigma);
					ROS_INFO("new: %f, %f, %f", cylinders[j][0], cylinders[j][1], sigmas[j]);

				}
				visualization_msgs::Marker m;
            			m.header.frame_id = "/world";
            			m.ns = "cylinder_marker";
            			m.id = j+1;
            			m.type = visualization_msgs::Marker::CYLINDER;
            			m.action = visualization_msgs::Marker::ADD;
            			m.pose.position.x = cylinders[j][0];//O(0);
                		m.pose.position.y = cylinders[j][1];//O(1);
                		m.pose.position.z = 0.0;//O(2);
            			m.scale.x = sigmas[j];
            			m.scale.y = sigmas[j];
            			m.scale.z = 0.5;
            			m.color.a = 0.5;
            			m.color.r = 0.0;
            			m.color.g = 1.0;
            			m.color.b = 1.0;
	   
            			marker_pub_.publish(m);
				
		    	}
		}

	public:
		TreasureDetection() : nh_("~") {
		    // Make sure TF is ready
		    ros::Duration(0.5).sleep();
		    int dim[2] = {100,100};
		    
		    signal_sub_ = nh_.subscribe<std_msgs::Float32>("signal",1,&TreasureDetection::signal_callback,this);	
		    marker_pub_ = nh_.advertise<visualization_msgs::Marker>("treasure_marker",1);
		    position_sub_ = nh_.subscribe<geometry_msgs::Point32>("position",1,&TreasureDetection::position_callback,this);
		}
		
};

int main(int argc, char * argv[]) 
{
    ros::init(argc,argv,"treasure_detection");
    TreasureDetection td;

    ros::spin();
    return 0;
}

