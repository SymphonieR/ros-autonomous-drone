#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float32.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_types.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#define OPENCV_TRAITS_ENABLE_DEPRECATED
#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include "obstacle_detection/Param.h"
#include <geometry_msgs/Point32.h>

class ObstacleDetection {
    protected:
        ros::Subscriber scan_sub_;
        ros::Publisher marker_pub_;
	ros::Publisher position_pub_;
        ros::Publisher obstacle_pub_;
	ros::Publisher outlier_pub;
        tf::TransformListener listener_;

        ros::NodeHandle nh_;
        std::string base_frame_;
        double max_range_;
        double tolerance;
        int n_samples; 

        pcl::PointCloud<pcl::PointXYZ> lastpc_;
	cv::Mat_<uint32_t> accumulator_xy;
    protected: // ROS Callbacks

        void pc_callback(const sensor_msgs::PointCloud2ConstPtr msg) {
            pcl::PointCloud<pcl::PointXYZ> temp;
            pcl::fromROSMsg(*msg, temp);
            // Make sure the point cloud is in the base-frame
            listener_.waitForTransform(base_frame_,msg->header.frame_id,msg->header.stamp,ros::Duration(1.0));
            pcl_ros::transformPointCloud(base_frame_,msg->header.stamp, temp, msg->header.frame_id, lastpc_, listener_);

            //
            unsigned int n = temp.size();
            std::vector<size_t> pidx;
            // First count the useful points
            for (unsigned int i=0;i<n;i++) {
                float x = temp[i].x;
                float y = temp[i].y;
                float d = hypot(x,y);
                //if (d < 1e-2) {
                    // Bogus point, ignore
                  //  continue;
                //}
                x = lastpc_[i].x;
                y = lastpc_[i].y;
                d = hypot(x,y);
                if (d > max_range_) {
                    // too far, ignore
                    continue;
                }
                pidx.push_back(i);
            }
            //pcl_ros::transformPointCloud("/world",msg->header.stamp, temp, msg->header.frame_id, lastpc_, listener_);
            //
            n = pidx.size();
            /*obstacle_detection::Param param;
            param.a = 0;
            param.b = 0;*/
	    std_msgs::Float32 obstacle_msg;
	    obstacle_msg.data = 0;

	    if(n!=0){
		for (unsigned int i=0; i<(unsigned)n; i++){
                        double x = lastpc_[pidx[i]].x;
                        double y = lastpc_[pidx[i]].y;			
			double z = lastpc_[pidx[i]].z;
		        if(z > -tolerance){
				obstacle_msg.data = 1;
                        	//param.a = 1;
				ROS_INFO("A:1, X:%f, Y:%f, Z:%f",x,y,z);
				break;
	                 }
			/*double distance = hypot(hypot(x,y),z);
			if(distance < 0.4){
				param.b = 1;
				ROS_INFO("B:1, X:%f, Y:%f, Z:%f, Distance:%f",x,y,z,distance);
			}	*/
		}
	    } 	
	    //param_pub_.publish(param);
	    obstacle_pub_.publish(obstacle_msg);

	    pcl::PointCloud<pcl::PointXYZ> origin;
	    origin.push_back (pcl::PointXYZ (0., 0., 0.));
	    origin.header = temp.header;
	    pcl_ros::transformPointCloud("/world",msg->header.stamp, origin, msg->header.frame_id, lastpc_, listener_);
	    ROS_INFO("Drone position: %f, %f, %f",lastpc_[0].x, lastpc_[0].y, lastpc_[0].z);
	    geometry_msgs::Point32 pt;
	    pt.x = lastpc_[0].x;
            pt.y = lastpc_[0].y;
	    pt.z = lastpc_[0].z;

	    position_pub_.publish(pt);
	}

    public:
        ObstacleDetection() : nh_("~") {
            nh_.param("base_frame",base_frame_,std::string("/body"));
            nh_.param("max_range",max_range_,5.0);
            nh_.param("n_samples",n_samples,1000);
            nh_.param("tolerance",tolerance,1.0);

            assert(n_samples > 0);

            // Make sure TF is ready
            ros::Duration(0.5).sleep();

            scan_sub_ = nh_.subscribe("scans",1,&ObstacleDetection::pc_callback,this);
            marker_pub_ = nh_.advertise<visualization_msgs::Marker>("floor_plane",1);
            //param_pub_ = nh_.advertise<obstacle_detection::Param>("obstacle", 1);
	    obstacle_pub_ = nh_.advertise<std_msgs::Float32>("obstacle", 1);
	    position_pub_ = nh_.advertise<geometry_msgs::Point32>("position", 1);
        }

};

int main(int argc, char * argv[]) 
{
    ros::init(argc,argv,"obstacle_detection");
    ObstacleDetection detection;

    ros::spin();
    return 0;
}


