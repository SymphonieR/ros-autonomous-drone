#!/usr/bin/env sh
# Compute the mean image from the imagenet training lmdb
# N.B. this is available in data/ilsvrc12

EXAMPLE=/home/GTL/srazafin/examples2/
DATA=/home/GTL/srazafin/dataset2
TOOLS=/home/gpu_user/caffe/build/tools
#TOOLS=/cs-share/pradalier/caffe/build/tools

$TOOLS/compute_image_mean $EXAMPLE/followshore_train_lmdb \
  $DATA/imagenet_mean_fast.binaryproto

echo "Done."
