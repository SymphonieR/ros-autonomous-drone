#!/usr/bin/env sh

TOOLS=/home/gpu_user/caffe/build/tools
MODEL=/home/GTL/srazafin/catkin_ws/src/shore_follower/models
#TOOLS=/cs-share/pradalier/caffe/build/tools
$TOOLS/caffe train \
    --solver=$MODEL/solver_fast.prototxt 2>&1 | tee log.txt
