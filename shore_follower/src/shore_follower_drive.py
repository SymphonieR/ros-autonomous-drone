#!/usr/bin/env python

import cv2
from cv_bridge import CvBridge
import rospy
from geometry_msgs.msg import Twist
import numpy as np
import tensorflow as tf
from sensor_msgs.msg import Image
from std_msgs.msg import Float32

print('All modules were imported successfully')
class Model:
	def __init__(self,ck_path):
		self.graph = tf.get_default_graph()
		with self.graph.as_default():
			self.model = tf.keras.models.load_model(ck_path)
	def predict(self,img):
		img = cv2.resize(img, (32,32))[:,:,::-1]
		x = np.expand_dims(img,0)
		with self.graph.as_default():
			command = self.model.predict(x)[0][0]*2 - 1
		return command
		

class ShoreFollowerDrive:
	def __init__(self):
		rospy.init_node('shore_follower')
		self.linear_vel_max = rospy.get_param("~linear_vel_max", 1.0)
		self.linear_vel_min = rospy.get_param("~linear_vel_min", 0.3)
		self.linear_vel = self.linear_vel_max
		self.linear_vel_z = rospy.get_param("~linear_vel_z", 0.1)
		self.twist_factor = rospy.get_param("~twist_factor", 0.3)
		self.transport = rospy.get_param("~transport", "raw")
		self.model_file = rospy.get_param("~model_file","adam.h5")
		self.model = Model(self.model_file)
		self.bridge = CvBridge()
		self.obstacle = False
		self.twist = Twist()
		self.z = 0.5
		self.x = 0
		self.state = False
		self.twist_pub = rospy.Publisher("twist", Twist, queue_size=1)
		self.image_sub = rospy.Subscriber("image", Image, self.image_callback)
		self.obstacle_sub = rospy.Subscriber("obstacle", Float32, self.obstacle_callback)
		self.metal_sub = rospy.Subscriber("metal", Float32, self.metal_callback)
		print('Initialization done')

	def metal_callback(self, metal_msg):
		pStart = 0.5
		pEnd = 0.9
		metal_signal = metal_msg.data
		if (metal_signal < pStart):
			self.linear_vel = self.linear_vel_max
		else:
			if (metal_signal < pEnd):
				alpha = (self.linear_vel_max - self.linear_vel_min)/(pStart-pEnd)
				beta = (self.linear_vel_max/pStart - self.linear_vel_min/pEnd)/(1/pStart - 1/pEnd)
				self.linear_vel = alpha*metal_signal + beta
			else: 
				self.linear_vel = self.linear_vel_min
 
	def obstacle_callback(self, obstacle_msg):
		if (obstacle_msg.data==0):
			self.obstacle = False;
		else: 
			self.obstacle = True;

	def image_callback(self, img_msg):
		#a faire:
		# ralentir a lapproche de la mine
		# detecter/eviter obstacle et drone?
		# synchronisation de 3 drones
		cv_image = self.bridge.imgmsg_to_cv2(img_msg)
		if (self.obstacle):
			self.state = 0
			self.twist_pub.publish(self.avoid_obstacle())
		elif (self.z > 0.5):
			if self.state<1.5:
				self.twist_pub.publish(self.go_straight()) 
			else:
				self.twist_pub.publish(self.go_down(cv_image))
		else:
			self.twist_pub.publish(self.predict(cv_image))

		rospy.loginfo("Linear: [%s, %s], Angular: %s", self.twist.linear.x, self.twist.linear.z, self.twist.angular.z)
	
	def go_straight(self):
		out = Twist()
		out.linear.x = self.linear_vel
		out.linear.z = self.z
		self.twist = out
		self.state += 0.05
		return out

	def go_down(self,cv_image):
		out = self.predict(cv_image)
		out.linear.x = self.linear_vel/5
		self.z -= 0.05
		if self.z <=0.5:
			self.z = 0.5
		out.linear.z = self.z
		self.twist = out
		return out
 
	def predict(self, img_msg):
		out = Twist()
		out.linear.x = self.linear_vel
		out.angular.z = self.model.predict(img_msg)
		out.linear.z = self.z
		self.twist = out
		return out

	def avoid_obstacle(self):
		out = Twist()
		self.z += 0.01
		out.linear.z = self.z
		
		self.twist = out
		return out

if __name__ == '__main__':
	try:
		SF = ShoreFollowerDrive()
		rospy.spin()
	except rospy.ROSInterruptException:
		pass

		
